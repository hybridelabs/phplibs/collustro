<?php
declare(strict_types=1);

namespace HybrideLabs\Collustro\Tests\Unit;

use HybrideLabs\Collustro\Facades\Export;
use HybrideLabs\Collustro\Models\Entry;
use HybrideLabs\Collustro\Models\Sets;
use HybrideLabs\Collustro\Tests\TestCase;
use Illuminate\Support\Collection;

class ExportTest extends TestCase
{

    protected array $questions = [
        [
            'content' => "Question #1",
            'type'    => "text",
            'options' => [],
        ],
        [
            'content' => "Question #2",
            'type'    => "radio",
            'options' => ["Radio #1", "Radio #2"],
        ],
        [
            'content' => "Question #3",
            'type'    => "number",
            'options' => [],
        ],
        [
            'content' => "Question #4",
            'type'    => "checkbox",
            'options' => ["Check #1", "Check #2", "Check #3"],
        ],
    ];

    /** @test */
    public function exportsCorrectData()
    {
        $set = Sets::create(['name' => "Test Survey", 'settings' => ['accepts-guest-entries' => true]]);
        foreach ($this->questions as $question) {
            $set->questions()->create($question);
        }

        (new Entry())
            ->for($set)
            ->fromArray(
                [
                    'q1' => "Mock Turtle. So.",
                    'q2' => 2,
                    'q3' => 8,
                    'q4' => [1, 3],
                ]
            )
            ->push();
        (new Entry())
            ->for($set)
            ->fromArray(
                [
                    'q1' => "Long Tale They.",
                    'q2' => 1,
                    'q3' => 12,
                    'q4' => [2],
                ]
            )
            ->push();

        $export = Export::byId($set->id)
                        ->asArray()
                        ->export();

        $expected = [
            0 => [
                'question' => 'Question #1',
                'type'     => 'text',
                'options'  => [],
                'answers'  => [
                    0 =>
                        [
                            'participant' => 'anonyme',
                            'value'       => 'Mock Turtle. So.',
                        ],
                    1 =>
                        [
                            'participant' => 'anonyme',
                            'value'       => 'Long Tale They.',
                        ],
                ],
            ],
            1 => [
                'question' => 'Question #2',
                'type'     => 'radio',
                'options'  =>
                    [
                        0 => 'Radio #1',
                        1 => 'Radio #2',
                    ],
                'answers'  =>
                    [
                        0 =>
                            [
                                'participant' => 'anonyme',
                                'value'       => '2',
                            ],
                        1 =>
                            [
                                'participant' => 'anonyme',
                                'value'       => '1',
                            ],
                    ],
            ],
            2 => [
                'question' => 'Question #3',
                'type'     => 'number',
                'options'  =>
                    [
                    ],
                'answers'  =>
                    [
                        0 =>
                            [
                                'participant' => 'anonyme',
                                'value'       => '8',
                            ],
                        1 =>
                            [
                                'participant' => 'anonyme',
                                'value'       => '12',
                            ],
                    ],
            ],
            3 => [
                'question' => 'Question #4',
                'type'     => 'checkbox',
                'options'  =>
                    [
                        0 => 'Check #1',
                        1 => 'Check #2',
                        2 => 'Check #3',
                    ],
                'answers'  =>
                    [
                        0 =>
                            [
                                'participant' => 'anonyme',
                                'value'       =>
                                    [
                                        0 => 1,
                                        1 => 3,
                                    ],
                            ],
                        1 =>
                            [
                                'participant' => 'anonyme',
                                'value'       =>
                                    [
                                        0 => 2,
                                    ],
                            ],
                    ],
            ],
        ];
        $this->assertIsArray($export);
        $this->assertEquals($expected, $export);
    }

    /**
     * @test
     */
    public function exportObjectValidWithSetsAsParameter()
    {
        $set = Sets::create(['name' => "Test Survey", 'settings' => ['accepts-guest-entries' => true]]);
        foreach ($this->questions as $question) {
            $set->questions()->create($question);
        }
        (new Entry())
            ->for($set)
            ->fromArray(
                [
                    'q1' => "Mock Turtle. So.",
                    'q2' => 2,
                    'q3' => 8,
                    'q4' => [1, 3],
                ]
            )
            ->push();
        $export = Export::by($set);

        $this->assertInstanceOf(\HybrideLabs\Collustro\Helpers\Export::class, $export);
    }

    /** @test */
    public function exportWithParticipantCallback()
    {
        $set = Sets::create(['name' => "Test Survey"]);
        foreach ($this->questions as $question) {
            $set->questions()->create($question);
        }
        (new Entry())
            ->for($set)
            ->by($this->user)
            ->fromArray(
                [
                    'q1' => "Mock Turtle. So.",
                    'q2' => 2,
                    'q3' => 8,
                    'q4' => [1, 3],
                ]
            )
            ->push();
        $callback = function ($arguments) {
            return $arguments[0] * $arguments[1];
        };

        $export = Export::by($set)->withHook("participant", $callback, 2);

        $this->assertInstanceOf(\HybrideLabs\Collustro\Helpers\Export::class, $export);

        $result = $export->export();

        $this->assertInstanceOf(Collection::class, $result);
        $this->assertTrue($result->first()->get("answers")->first()->get("participant") == $this->user->id * 2);
    }
}
