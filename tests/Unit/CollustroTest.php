<?php
declare(strict_types=1);

namespace HybrideLabs\Collustro\Tests\Unit;

use HybrideLabs\Collustro\Exceptions\MissingParametersException;
use HybrideLabs\Collustro\Models\Sets;
use HybrideLabs\Collustro\Tests\TestCase;

class CollustroTest extends TestCase
{

    /** @test */
    public function throwsExceptionWhenCalledWithoutParameters()
    {
        $this->expectException(MissingParametersException::class);
        Sets::create();
    }

    /** @test */
    public function throwsExceptionWhenCalledWithoutNameParameter()
    {
        $this->expectException(MissingParametersException::class);
        Sets::create(['something' => 'non-existing']);
    }

    /** @test */
    public function createsValidCollustroSetsObject()
    {
        $name      = "Collustro test survey";
        $collustro = Sets::create(["name" => $name]);

        $this->assertInstanceOf(Sets::class, $collustro);
        $this->assertTrue($collustro->name == $name);
    }

    /** @return array */
    public function constraintsProvider()
    {
        return [
            [json_encode(["attribute" => "accept-guest-entries", "value" => true])],
            [json_encode(["attribute" => "limit-per-participant", "value" => 1])],
        ];
    }

    /**
     * @test
     * @dataProvider constraintsProvider
     */
    public function createsValidCollustroSetsObjectsWithConstraints($data)
    {
        $name      = "Collustro test survey";
        $data      = json_decode($data, true);
        $collustro = Sets::create(["name" => $name, $data["attribute"] => $data["value"]]);

        $this->assertInstanceOf(Sets::class, $collustro);
    }

}
