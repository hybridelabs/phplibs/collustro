<?php
declare(strict_types=1);

namespace HybrideLabs\Collustro\Tests;

use HybrideLabs\Collustro\CollustroServiceProvider;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Orchestra\Testbench\TestCase as TestCaseBase;

class TestCase extends TestCaseBase
{
    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->app->make(EloquentFactory::class)->load(__DIR__.'/database/factories');
        $this->user = factory(User::class)->make();
    }

    protected function getPackageProviders($app)
    {
        return [
            CollustroServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetup($app)
    {
        include_once __DIR__.'/database/migrations/create_users_table.php.stub';
        (new \CreateUsersTable())->up();

        include_once __DIR__.'/../database/migrations/create_answers_table.php.stub';
        include_once __DIR__.'/../database/migrations/create_entries_table.php.stub';
        include_once __DIR__.'/../database/migrations/create_questions_table.php.stub';
        include_once __DIR__.'/../database/migrations/create_sections_table.php.stub';
        include_once __DIR__.'/../database/migrations/create_sets_table.php.stub';
        (new \CreateAnswersTable())->up();
        (new \CreateEntriesTable())->up();
        (new \CreateQuestionsTable())->up();
        (new \CreateSectionsTable())->up();
        (new \CreateSetsTable())->up();
    }
}
