# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.9.6][0.9.6] - 2020-05-13
### Fixed
- Relation of User model to Entry model, flipped the identifiers

## 0.9.5 - 2020-04-29
### Modified
- Removed Facade for Entry
- Switched to Export facade in unit tests

## 0.9.4 - 2020-04-29
### Added
- Export hooks for modifying values on the fly (only participant for the moment)
- User class to unit test

### Fixed
- Correct saving of the participant id
- Fixed test factory for User class

## 0.9.3 - 2020-04-29
### Added
- Possibility to load the export with a **Sets** instance
- New exception for wrong object type

### Modified
- Various exceptions in the **Export**

## 0.9.3 - 2020-04-29
### Added
- Exporting data as a collection or array
- Unit test for export as array
- Facade for Export
- Facade for Entry

### Modified
- Relation name from survey to set to keep the naming coherent
- Unit test base class

## 0.9.2 - 2020-04-27
### Modified
- Type column declaration of table blueprint for questions

## 0.9.1 - 2020-04-24
### Fixed
- Null value exception when trying to get values from non-existing array

## 0.9.0
### Added
- Clean code base
- Basic views
- Basic unit tests

### Untested
- Sections

### Todo
- Creating more tests
- Views for creating a surveys
- Translations for all texts

[0.9.6]: https://gitlab.com/hybridelabs/phplibs/collustro/-/commit/477b325a45a4b010ca05c23276ac5eef987317df