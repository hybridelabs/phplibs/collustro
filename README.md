# Collustro

Surveys made easy for Laravel

## Installation

Require the package with composer:
```sh
composer require hybridelabs/collustro
```

Publish the migrations (required), the configuration (optional) and views (recommended):
```sh
php artisan vendor:publish --provider="HybrideLabs\Collustro\CollustroServiceProvider" --tag="migrations"
php artisan vendor:publish --provider="HybrideLabs\Collustro\CollustroServiceProvider" --tag="config"
php artisan vendor:publish --provider="HybrideLabs\Collustro\CollustroServiceProvider" --tag="views"
```

Run the migrations to create the required tables:
```sh
php artisan migrate
```

## Usage

### Create a survey

A simple one:
```php
$collustro = Sets::create(["name" => "Animals"]);
$collustro->questions()->create([
    "content" => "Your favorite pet?",
    "type"    => "text",
]);
```

### Add an Entry

From an array:
```php
(new Entry())->for($collustro)->fromArray([
    "q1" => "My kitty."
]);
```
