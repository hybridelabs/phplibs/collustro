<h3 class="px-4 py-2">
    {{ $section->name }}
</h3>
@foreach($section->questions as $question)
    @include('collustro::questions.single')
@endforeach
