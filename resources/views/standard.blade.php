<div class="card">
    <div class="card-header bg-white p-4">
        @if( ! $eligible)
            We accept only
            <strong>{{ $collustro->limitPerParticipant() }} {{ \Str::plural('participation', $collustro->limitPerParticipant()) }}</strong>
            per user.
        @endif

        @if($lastEntry)
            Your last entry was <strong>{{ $lastEntry->created_at->diffForHumans() }}</strong>.
        @endif

    </div>
    @if(!$survey->acceptsGuestEntries() && auth()->guest())
        <div class="p-5">
            Please login to participate.
        </div>
    @else
        @foreach($survey->sections as $section)
            @include('collustro::sections.single')
        @endforeach

        @foreach($survey->questions()->withoutSection()->get() as $question)
            @include('collustro::questions.single')
        @endforeach

        @if($eligible)
            <button class="btn btn-primary">Submit</button>
        @endif
    @endif
</div>
