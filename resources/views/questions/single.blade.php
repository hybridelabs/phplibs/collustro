<div class="p-4 border-bottom">
    @php($type = view()->exists("collustro::questions.types.".$question->type) ? $question->type : "text")
    @include("collustro::questions.types.".$type, [
        'disabled' => ! ($eligible ?? true),
        'value' => $lastEntry ? $lastEntry->answerFor($question) : null,
        'includeResults' => ! is_null($lastEntry ?? null),
    ])
</div>
