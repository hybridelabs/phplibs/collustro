@component('collustro::questions.base', compact('question'))
    <input type="number"
           name="{{ $question->key }}"
           id="{{ $question->key }}"
           class="form-control"
           value="{{ $value ?? old($question->key) }}"
        {{ ($disabled ?? false) ? 'disabled' : '' }}
    />

    @slot('report')
        @if($includeResults ?? false)
            {{ number_format((new \HybrideLabs\Collustro\Helpers\Summary($question))->average()) }} (Average)
        @endif
    @endslot
@endcomponent
