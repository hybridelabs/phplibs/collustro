<?php
declare(strict_types=1);

return [
    /** Database related configuration. */
    'database' => [
        /** Table names */
        'tables' => [
            'sets'      => 'collustro_sets',
            'sections'  => 'collustro_sections',
            'questions' => 'collustro_questions',
            'answers'   => 'collustro_answers',
            'entries'   => 'collustro_entries',
        ],
    ],

];
