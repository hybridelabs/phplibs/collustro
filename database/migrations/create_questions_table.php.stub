<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            config('collustro.database.tables.questions'),
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('set_id')->nullable();
                $table->unsignedBigInteger('section_id')->nullable();
                $table->string('content');
                $table->char('type', 16)->default('text');
                $table->text('options')->nullable();
                $table->text('rules')->nullable();
                $table->timestamps();

                $table->foreign('set_id')->references('id')->on(config('collustro.database.tables.sets'))
                      ->onDelete('cascade');

                $table->foreign('section_id')->references('id')->on(config('collustro.database.tables.sections'))
                      ->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('collustro.database.tables.questions'));
    }
}
