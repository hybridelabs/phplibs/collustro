<?php
declare(strict_types=1);

namespace HybrideLabs\Collustro\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Export
 *
 * @package HybrideLabs\Collustro\Facades
 */
class Export extends Facade
{

    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'export';
    }

}
