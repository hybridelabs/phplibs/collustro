<?php
declare(strict_types=1);

namespace HybrideLabs\Collustro\Models;

use HybrideLabs\Collustro\Exceptions\GuestEntriesNotAllowedException;
use HybrideLabs\Collustro\Exceptions\ParticipationLimitExceeded;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Entry
 *
 * @property HasMany answers
 * @property int id
 * @property Sets set
 * @property int set_id
 * @property int|null participant_id
 *
 * @package HybrideLabs\Collustro\Models
 *
 * @method static where(string $string, $participant_id)
 * @method count()
 */
class Entry extends Model
{

    /** @var string|null $table */
    protected $table;

    /** @inheritDoc */
    protected $dateFormat = 'Y-m-d H:i:s';

    /** @var string[] $fillable */
    protected $fillable = [
        "set_id",
        "participant_id",
    ];

    /** @var bool $hasParticipant */
    private bool $hasParticipant = false;

    /**
     * Entry constructor.
     *
     * @param  array  $attributes
     */
    public function __construct(array $attributes = [])
    {
        if ( ! isset($this->table)) {
            /** @var string $table */
            $table = config("collustro.database.tables.entries");
            $this->setTable($table);
        }

        parent::__construct($attributes);
    }

    /**
     * Load the model.
     *
     * @return void
     */
    protected static function boot(): void
    {
        parent::boot();

        // Prevent submissions that don't meet the constraints.
        static::creating(
            function (self $entry) {
                $entry->validateParticipant();
                $entry->validateParticipationLimitRequirement();
            }
        );
    }

    /**
     * Verify legibility of participant.
     *
     * @throws GuestEntriesNotAllowedException
     */
    public function validateParticipant(): void
    {
        /** @var Sets $this->set */
        $set = $this->set;
        if ( ! $set->acceptsGuestEntries() && is_null($this->participant)) {
            throw new GuestEntriesNotAllowedException();
        }
    }

    /**
     * Verify if entry exceeds participation limit.
     *
     * @throws ParticipationLimitExceeded
     */
    public function validateParticipationLimitRequirement(): void
    {
        /** @var Sets $set */
        $set = $this->set;
        /** @var ?int $limit */
        $limit = $set->participationLimit();

        if ( ! is_null($limit)) {
            /**
             * @var Entry $entry
             * @var int $this->participant->id
             */
            $entry = static::where("participant_id", $this->participant->id);
            $entry = $entry->where("set_id", $this->set_id);
            /** @var int $count */
            $count = $entry->count();

            if ($count >= $limit) {
                throw new ParticipationLimitExceeded();
            }
        }
    }

    /**
     * Save the model and all relationships; ensure the answers are automatically linked to the entry.
     *
     * @return bool
     */
    public function push(): bool
    {
        /** @var int $this->set->id */
        $this->set_id = $this->set->id;

        if ($this->hasParticipant) {
            /** @psalm-suppress MixedPropertyFetch */
            $this->participant_id = $this->participant->id ?? null;
        }
        $this->save();

        /** @var Answer $answer */
        foreach ($this->answers as $answer) {
            $answer->entry_id = $this->id;
        }

        return parent::push();
    }

    /**
     * Associate the Sets set to the entry.
     *
     * @param  Sets  $set
     *
     * @return $this
     */
    public function for(Sets $set): self
    {
        $this->set()->associate($set);

        return $this;
    }

    /**
     * Sets the set the entry belongs to.
     *
     * @return BelongsTo
     */
    public function set(): BelongsTo
    {
        return $this->belongsTo(Sets::class, "id", "set_id");
    }

    /**
     * Set participant for the entry.
     *
     * @param  Model  $participant
     *
     * @return $this
     */
    public function by(Model $participant): self
    {
        $this->hasParticipant = true;

        $this->participant(get_class($participant))->associate($participant);

        return $this;
    }

    /**
     * Participant of the entry.
     *
     * @param  string  $class
     *
     * @return BelongsTo
     */
    public function participant(string $class): BelongsTo
    {
        return $this->belongsTo($class, "participant_id", "id");
    }

    /**
     * Create an entry from an array.
     *
     * @param  array  $values
     *
     * @return $this
     */
    public function fromArray(array $values): self
    {
        /**
         * @var string $key
         * @var ?string $value
         */
        foreach ($values as $key => $value) {
            if (is_null($value)) {
                continue;
            }
            $questionId = (int) substr((string) $key, 1);

            /** @var Question $question */
            $question = Question::find($questionId);

            if ($question->type == "checkbox") {
                $value = json_encode($value);
            }
            /** @var BelongsTo $answers */
            $answers = $this->answers;
            $answers->add(
                Answer::make(
                    [
                        "question_id" => $questionId,
                        "entry_id"    => null,
                        "value"       => $value,
                        "type"        => $question->type,
                    ]
                )
            );
        }

        return $this;
    }

    /**
     * Answer to a question.
     *
     * @param  Question  $question
     *
     * @return array<string>|string|null
     */
    public function answerFor(Question $question)
    {
        $answer = $this->answers()->where("question_id", $question->id)->first();

        /** @var ?string $value */
        $value = isset($answer) ? $answer->value : null;

        return $value;
    }

    /**
     * Answers within the entry.
     *
     * @return HasMany
     */
    public function answers(): HasMany
    {
        return $this->hasMany(Answer::class);
    }
}
