<?php
declare(strict_types=1);

namespace HybrideLabs\Collustro\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Section
 *
 * @property int set_id
 *
 * @package HybrideLabs\Sets\Models
 */
class Section extends Model
{

    /** @var string|null $table */
    protected $table;

    /** @inheritDoc */
    protected $dateFormat = 'Y-m-d H:i:s';

    /** @var string[] $fillable */
    protected $fillable = [
        "name",
        "set_id",
    ];

    /**
     * Section constructor.
     *
     * @param  array  $attributes
     */
    public function __construct(array $attributes = [])
    {
        if ( ! isset($this->table)) {
            /** @var string $table */
            $table = config("collustro.database.tables.sections");
            $this->setTable($table);
        }

        parent::__construct($attributes);
    }

    /**
     * Questions in the section.
     *
     * @return HasMany
     */
    public function questions(): HasMany
    {
        return $this->hasMany(Question::class);
    }

}
