<?php
declare(strict_types=1);

namespace HybrideLabs\Collustro\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Answer
 *
 * @property int entry_id
 * @property string type
 * @property string value
 *
 * @package HybrideLabs\Collustro\Models
 *
 * @method static make(array $array)
 *
 */
class Answer extends Model
{

    /** @var ?string $table */
    protected $table;

    /** @inheritDoc */
    protected $dateFormat = 'Y-m-d H:i:s';

    /** @var string[] $fillable */
    protected $fillable = [
        "value",
        "type",
        "question_id",
        "entry_id",
    ];

    /**
     * Answer constructor.
     *
     * @param  array  $attributes
     */
    public function __construct(array $attributes = [])
    {
        if ( ! isset($this->table)) {
            /** @var string $table */
            $table = config("collustro.database.tables.answers");
            $this->setTable($table);
        }

        parent::__construct($attributes);
    }

    /**
     * The entry the answer belongs to.
     *
     * @return BelongsTo
     */
    public function entry(): BelongsTo
    {
        return $this->belongsTo(Entry::class);
    }

    /**
     * The question the answer belongs to.
     *
     * @return BelongsTo
     */
    public function question(): BelongsTo
    {
        return $this->belongsTo(Question::class);
    }

    /**
     * @param  string  $value
     *
     * @return mixed
     */
    public function getValueAttribute(string $value)
    {
        if ($this->type == "checkbox") {
            /** @var array $value */
            $value = json_decode($value, true);
        }

        return $value;
    }

}
