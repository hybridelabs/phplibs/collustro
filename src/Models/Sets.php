<?php
declare(strict_types=1);

namespace HybrideLabs\Collustro\Models;

use HybrideLabs\Collustro\Exceptions\MissingParametersException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Sets
 *
 * @property array<Question> questions
 * @property int id
 * @property Sets set
 *
 * @package HybrideLabs\Sets\Models
 *
 * @method where(string $string, $null)
 * @method find(int $id)
 * @method static create(array $array)
 */
class Sets extends Model
{

    /** @var ?string $table */
    protected $table;

    /** @inheritDoc */
    protected $dateFormat = 'Y-m-d H:i:s';

    /** @var string[] $fillable */
    protected $fillable = [
        "name",
        "settings",
    ];

    /** @var array $casts */
    protected $casts = [
        "settings" => "array",
    ];

    /**
     * Sets constructor.
     *
     * @param  array  $attributes
     */
    public function __construct(array $attributes = [])
    {
        if ( ! isset($this->table)) {
            /** @var string $table */
            $table = config("collustro.database.tables.sets");
            $this->setTable($table);
        }

        parent::__construct($attributes);
    }

    /**
     * @throws MissingParametersException
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(
            function (self $sets) {
                if (empty($sets->attributes) || is_null($sets->attributes['name'])) {
                    throw new MissingParametersException();
                }
            }
        );
    }

    /**
     * Survey sections.
     *
     * @return HasMany
     */
    public function sections(): HasMany
    {
        return $this->hasMany(Section::class, "set_id");
    }

    /**
     * Survey questions.
     *
     * @return HasMany
     */
    public function questions(): HasMany
    {
        return $this->hasMany(Question::class, "set_id");
    }

    /**
     * Last submitted entry by participants.
     *
     * @param  Model  $participant
     *
     * @return null|object
     */
    public function lastEntry(Model $participant = null): ?object
    {
        /**
         * @var HasMany|null $lastEntry
         */
        return $lastEntry = (is_null($participant) ? null : $this->entriesFrom($participant)->first());
    }

    /**
     * All entries by participants.
     *
     * @param  Model  $participant
     *
     * @return HasMany
     */
    public function entriesFrom(Model $participant): HasMany
    {
        return $this->entries()->where("participant_id", $participant->id);
    }

    /**
     * Saved entries.
     *
     * @return HasMany
     */
    public function entries(): HasMany
    {
        return $this->hasMany(Entry::class, "set_id", "id");
    }

    /**
     * Check if eligible for submission.
     *
     * @param  Model|null  $participant
     *
     * @return bool
     */
    public function isEligible(?Model $participant = null): bool
    {
        if (is_null($participant)) {
            return $this->acceptsGuestEntries();
        }

        if (is_null($this->participationLimit())) {
            return true;
        }

        return $this->participationLimit() > $this->entriesFrom($participant)->count();
    }

    /**
     * Check if guest entries are accepted.
     *
     * @return bool
     */
    public function acceptsGuestEntries(): bool
    {
        /** @var bool $value */
        $value = $this->settings['accepts-guest-entries'] ?? false;

        return $value;
    }

    /**
     * The maximum number of entries by participants.
     *
     * @return int|null
     *
     * @todo reworking this piece of code.
     */
    public function participationLimit(): ?int
    {
        if ($this->acceptsGuestEntries()) {
            return null;
        }
        /** @var int $limit */
        $limit = $this->settings["participation-limit"] ?? 1;
        /** @var int|null $limit */
        $limit = ($limit !== -1) ? $limit : null;

        return $limit;
    }

    /**
     * Validation rules.
     *
     * @return array
     */
    public function getRulesAttribute(): array
    {
        assert($this->questions instanceof Question);
        /** @var Collection $mapped */
        $mapped = $this->questions->mapWithKeys(
            function (Question $question) {
                /**
                 * @var string $question->key
                 * @var string $question->rules
                 */
                return [$question->key => $question->rules];
            }
        );
        /** @var array $all */
        $all = $mapped->all();

        return $all;
    }
}
