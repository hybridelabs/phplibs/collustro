<?php
declare(strict_types=1);

namespace HybrideLabs\Collustro\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Question
 *
 * @package HybrideLabs\Sets\Models
 *
 * @property int id
 * @property Section section
 * @property int set_id
 * @property string type
 * @property array<Answer> answers
 */
class Question extends Model
{

    /** @var string|null $table */
    protected $table;

    /** @inheritDoc */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**  @var string[] $fillable */
    protected $fillable = [
        "type",
        "options",
        "content",
        "rules",
        "set_id",
    ];

    /** @var string[] $casts */
    protected $casts = [
        "rules"   => "array",
        "options" => "array",
    ];

    /**
     * Question constructor.
     *
     * @param  array  $attributes
     */
    public function __construct(array $attributes = [])
    {
        if ( ! isset($this->table)) {
            /** @var string $table */
            $table = config("collustro.database.tables.questions");
            $this->setTable($table);
        }

        parent::__construct($attributes);
    }

    /**
     * Loading the model.
     *
     * @return void
     */
    protected static function boot(): void
    {
        parent::boot();

        // Ensure the survey for the question is the same as the section.
        static::creating(
            function (self $question) {
                $question->load("section");

                if ($question->section) {
                    /** @var Section $section */
                    $section = $question->section;
                    /** @var int set_id */
                    $question->set_id = $section->set_id;
                }
            }
        );
    }

    /**
     * The survey the question belongs to.
     *
     * @return BelongsTo
     */
    public function set(): BelongsTo
    {
        return $this->belongsTo(Sets::class);
    }

    /**
     * The section the question belongs to.
     *
     * @return BelongsTo
     */
    public function section(): BelongsTo
    {
        return $this->belongsTo(Section::class);
    }


    /**
     * The answers that belong to the question.
     *
     * @return HasMany
     */
    public function answers(): HasMany
    {
        return $this->hasMany(Answer::class);
    }

    /**
     * Question validation rules.
     *
     * @param  mixed|null  $value
     *
     * @return mixed
     *
     * @todo "mixed" return type is a no-no, destructuring the methods to have clear return types is necessary.
     */
    public function getRulesAttribute(?mixed $value = null)
    {
        /** @var mixed $value */
        $value = $this->castAttribute("rules", $value);

        return is_null($value) ? [] : $value;
    }

    /**
     * Unique key representing the question.
     *
     * @return string
     */
    public function getKeyAttribute(): string
    {
        return "q{$this->id}";
    }

    /**
     * Scope a query to only include questions that don't belong to any sections.
     *
     * @param  Builder  $query
     *
     * @return mixed
     *
     * @todo "mixed" return type is a no-no, destructuring the methods to have clear return types is necessary.
     */
    public function scopeWithoutSection(Builder $query)
    {
        return $query->where("section_id", null);
    }
}
