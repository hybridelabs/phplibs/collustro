<?php
declare(strict_types=1);

namespace HybrideLabs\Collustro\Helpers;

use HybrideLabs\Collustro\Models\Question;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Summary
 *
 * @package HybrideLabs\Sets
 */
class Summary
{
    /** @var Question $question */
    protected Question $question;

    /**
     * Summary constructor.
     *
     * @param  Question  $question
     */
    public function __construct(Question $question)
    {
        $this->question = $question;
    }

    /**
     * Find the ratio of similar answers to all other answers.
     *
     * @param  string  $value
     *
     * @return float
     *
     * @todo Allow for checkboxes as well (array-based), maybe reworking the answers table?
     */
    public function similarAnswersRatio(string $value): float
    {
        $total = (float) $this->question->answers()->count();

        return $total > 0.0 ? $this->similarAnswers($value)->count() / $total : 0.0;
    }

    /**
     * Find all answers with the same value.
     *
     * @param $value
     *
     * @return HasMany
     */
    public function similarAnswers(string $value)
    {
        return $this->question->answers()->where('value', $value);
    }

    /**
     * Calculate the average answer.
     *
     * @return int|mixed
     */
    public function average()
    {
        return $this->question->answers()->average('value') ?? 0;
    }
}
