<?php
declare(strict_types=1);

namespace HybrideLabs\Collustro\Helpers;

use Closure;
use HybrideLabs\Collustro\Exceptions\InvalidTypeException;
use HybrideLabs\Collustro\Models\Answer;
use HybrideLabs\Collustro\Models\Entry;
use HybrideLabs\Collustro\Models\Question;
use HybrideLabs\Collustro\Models\Sets;
use Illuminate\Support\Collection;
use InvalidArgumentException;

/**
 * @property Sets set
 */
class Export
{

    /** @var Sets|null $set */
    private ?Sets $set = null;

    /** @var bool $asArray */
    private bool $asArray = false;

    /** @var array<string, array<string, mixed>> $hooks */
    private array $hooks = [];

    /**
     * @param  Sets|null  $set
     *
     * @return $this
     * @throws InvalidTypeException
     */
    public function by(?Sets $set = null): self
    {
        /** @psalm-suppress TypeDoesNotContainType */
        if (is_null($set)) {
            throw new InvalidArgumentException("Set cannot be null.");
        } elseif ( ! $set instanceof Sets) {
            throw new InvalidTypeException("Object must be of type Set.");
        }
        $this->set = $set;

        return $this;
    }

    /**
     * @param  int  $id
     *
     * @return Export
     */
    public function byId(?int $id = null): self
    {
        if (is_null($id)) {
            throw new InvalidArgumentException("Id cannot be null.");
        }
        /** @var ?Sets $set */
        $set = Sets::find($id);

        if (is_null($set)) {
            throw new InvalidArgumentException("Set with id '$id' does not exist.");
        }
        $this->set = $set;

        return $this;
    }

    /**
     * @return $this
     */
    public function asArray(): self
    {
        $this->asArray = true;

        return $this;
    }

    /**
     * @param  string  $hook
     * @param  Closure  $callback
     * @param  array  $arguments
     *
     * @return Export
     */
    public function withHook(?string $hook = null, ?Closure $callback = null, ...$arguments): self
    {
        if (is_null($hook) || is_null($callback)) {
            throw new InvalidArgumentException();
        }
        $this->hooks[$hook] = [
            "callback"  => $callback,
            "arguments" => $arguments,
        ];

        return $this;
    }

    /**
     * @psalm-suppress MixedInferredReturnType
     * @return Collection|array
     */
    public function export()
    {
        $export = collect();

        /** @var ?Sets $set */
        $set = $this->set;
        if (is_null($set)) {
            return $export;
        }
        /** @var ?array<Question> $questions */
        $questions = $set->questions;

        if ( ! is_null($questions)) {
            /** @var Question $question */
            foreach ($questions as $key => $question) {
                $answers = collect();
                /** @var Answer $answer */
                foreach ($question->answers as $index => $answer) {
                    /** @var Entry $relatedEntry */
                    $relatedEntry = $answer->entry()->first();

                    // Hook for participant
                    $participant = "anonyme";
                    if ( ! is_null($relatedEntry->participant_id ?? null)) {
                        $participant = $this->hook("participant", (string) $relatedEntry->participant_id);
                    }
                    $preparation = [
                        "participant" => $participant,
                        "value"       => $answer->value,
                    ];
                    if ( ! $this->asArray) {
                        $preparation = collect($preparation);
                    }
                    $answers->push($preparation);
                }
                if ($this->asArray) {
                    $answers = $answers->toArray();
                }
                $entry = [
                    "question" => $question->content,
                    "type"     => $question->type,
                    "options"  => $question->options,
                    "answers"  => $answers,
                ];
                if ( ! $this->asArray) {
                    $entry = collect($entry);
                }
                $export->push($entry);
            }
        }
        if ($this->asArray) {
            $export = $export->toArray();
        }

        return $export;
    }

    /**
     * @param  string  $hook
     * @param  string|null  $value
     *
     * @return string|null
     *
     * @psalm-suppress MixedInferredReturnType
     * @psalm-suppress MixedReturnStatement
     */
    private function hook(string $hook, ?string $value = null): ?string
    {
        if (array_key_exists($hook, $this->hooks)) {
            /** @var Closure $callback */
            $callback = $this->hooks[$hook]["callback"];
            /** @psalm-suppress MixedArgument */
            $arguments = array_merge([$value], $this->hooks[$hook]["arguments"]);

            return (string) $callback($arguments);
        }

        return $value;
    }

}
