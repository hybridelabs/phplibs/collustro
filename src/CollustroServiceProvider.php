<?php
declare(strict_types=1);

namespace HybrideLabs\Collustro;

use HybrideLabs\Collustro\Helpers\Export;
use HybrideLabs\Collustro\Http\View\Composers\CollustroComposer;
use HybrideLabs\Collustro\Models\Entry;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class CollustroServiceProvider extends ServiceProvider
{
    protected array $migrations = [
        0 => "create_sets_table",
        1 => "create_sections_table",
        2 => "create_questions_table",
        3 => "create_entries_table",
        4 => "create_answers_table",
    ];

    public function register(): void
    {
        $this->app->bind(
            'export',
            function () {
                return new Export();
            }
        );
        $this->app->bind(
            'entry',
            function () {
                return new Entry();
            }
        );

        $this->mergeConfigFrom(__DIR__."/../config/collustro.php", "collustro");
        $this->loadViewsFrom(__DIR__."/../resources/views", "collustro");
    }

    /**
     * @param  ViewFactory  $viewFactory
     */
    public function boot(ViewFactory $viewFactory): void
    {
        if ($this->app->runningInConsole()) {
            $this->publishes(
                [
                    __DIR__."/../config/collustro.php" => config_path("collustro.php"),
                ],
                "config"
            );
            $this->publishes(
                [
                    __DIR__."/../resources/views/" => base_path("resources/views/vendor/collustro"),
                ],
                "views"
            );
            /**
             * @var int $index
             * @var string $migration
             */
            foreach ($this->migrations as $index => $migration) {
                /** @var string $migrationClass */
                $migrationClass = Str::studly($migration);

                if (class_exists($migrationClass)) {
                    return;
                }

                $this->publishes(
                    [
                        __DIR__."/../database/migrations/$migration.php.stub" => database_path(
                            "migrations/".date("Y_m_d_Hi", time())."0".$index."_$migration.php"
                        ),
                    ],
                    "migrations"
                );
            }
        }

        $viewFactory->composer("collustro::standard", CollustroComposer::class);
    }

}
