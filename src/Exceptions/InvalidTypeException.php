<?php
declare(strict_types=1);

namespace HybrideLabs\Collustro\Exceptions;

use Exception;

class InvalidTypeException extends Exception
{

    /**
     * The exception message.
     *
     * @var string $message
     */
    protected $message = "Submitted object must be of type Sets.";

    public function __construct(?string $message = null)
    {
        if ( ! is_null($message)) {
            $this->message = $message;
        }
    }

}
