<?php
declare(strict_types=1);

namespace HybrideLabs\Collustro\Exceptions;

use Exception;

class MissingParametersException extends Exception
{
    /**
     * The exception message.
     *
     * @var string $message
     */
    protected $message = "Missing parameter for Sets. At least a name for the set is needed.";

}
