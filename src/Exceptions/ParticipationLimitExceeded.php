<?php
declare(strict_types=1);

namespace HybrideLabs\Collustro\Exceptions;

use Exception;

class ParticipationLimitExceeded extends Exception
{
    /**
     * The exception message.
     *
     * @var string
     */
    protected $message = "Participation limit for user exceeded.";
}
