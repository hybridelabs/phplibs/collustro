<?php
declare(strict_types=1);

namespace HybrideLabs\Collustro\Exceptions;

use Exception;

class GuestEntriesNotAllowedException extends Exception
{
    /**
     * The exception message.
     *
     * @var string $message
     */
    protected $message = "Login required for participation.";

}
