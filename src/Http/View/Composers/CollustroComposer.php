<?php
declare(strict_types=1);

namespace HybrideLabs\Collustro\Http\View\Composers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\View\View;

class CollustroComposer
{
    protected $auth;

    /**
     * CollustroComposer constructor.
     *
     * @param  Guard  $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Compose the view with relevant values.
     *
     * @param  View  $view
     */
    public function compose(View $view)
    {
        $view->with(
            [
                'eligible'  => $view->collustro->isEligible($this->auth->user()),
                'lastEntry' => $view->collustro->lastEntry(auth()->user()),
            ]
        );
    }
}
